<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="main-content">
 */
?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <!-- <title><?php wp_title( '|', true, 'right' ); ?></title> -->
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="shortcut icon" href="<?php echo of_get_option('favicon'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/includes/js/html5.js"></script>
    <![endif]-->
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <?php do_action('before'); ?>
        <header id="masthead" class="site-header" role="banner">
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mail-info">
                                <?php if (of_get_option('phone_number')): ?>
                                <span class="phone-info">
                                    <i class="fa fa-phone"></i>
                                    <?php echo of_get_option('phone_number'); ?>
                                </span>
                                <?php endif; ?>
                                <?php if (of_get_option('email_id')): ?>
                                <span>
                                    <i class="fa fa-envelope"></i>
                                    <a href="mailto:<?php echo of_get_option('email_id'); ?>">
                                        <?php echo of_get_option('email_id'); ?>
                                    </a>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <!-- .col-sm-6-->
                        <div class="col-sm-6">
                            <div class="header-social-icon-wrap">
                                <ul class="social-icons">
                                    <?php
                                    echo '<a style="color:black; border-right: 1px solid black; padding-right:15px; padding-left:15px; text-decoration:none;" href="#">About Us</a>';
                                    echo '<a style="color:black; border-right: 1px solid black; padding-right:15px; padding-left:15px; text-decoration:none;" href="#">Careers</a>';
                                    echo '<a style="color:black; border-right: 1px solid black; padding-right:15px; padding-left:15px; text-decoration:none;" href="#">Contact Us</a>';
                                    echo '<a href="#"><img style="border-right: 1px solid black;padding-right:15px; padding-left:15px; margin-right :15px;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2017/10/13092155/google_play_logo.png" alt="gogle"></a>';
                            $socialmedia_navs = ascent_socialmedia_navs();
                            foreach ($socialmedia_navs as $socialmedia_url => $socialmedia_icon) {
                                if (of_get_option($socialmedia_url)) {
                                    echo '<li class="social-icon"><a target="_blank" href="'.of_get_option($socialmedia_url).'"><i class="'.$socialmedia_icon.'"></i></a></li>';
                                }
                            }
                            ?>
                                </ul>
                            </div>
                            <!--.header-social-icon-wrap-->
                        </div>
                        <!-- .col-sm-6-->
                    </div>
                </div>
            </div>
            <div id="header-main" class="header-bottom">
                <div class="header-bottom-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="http://localhost/wordpress/wp-content/uploads/2018/03/logo.png" />
                            </div>
                            <!--.col-sm-3-->

                            <div class="col-sm-8">
                                <div class="site-navigation pull-right">
                                    <nav class="main-menu">
                                        <?php
                            wp_nav_menu(array(
                                'theme_location' => 'main-menu',
                                'container' => false,
                                'menu_class' => 'header-nav clearfix',
                            ));
                            ?>
                                    </nav>
                                    <div id="responsive-menu-container"></div>
                                </div>
                                <!-- .site-navigation -->
                            </div>
                            <!--.col-sm-8-->
                            <div class="col-sm-1">
                                <img src="http://localhost/wordpress/wp-content/uploads/2018/03/live.png" style="height:auto;width:70px;" />
                            </div>
                        </div>
                        <!--.row-->
                    </div>
                    <!-- .container -->
                </div>
                <!--.header-bottom-inner-->
            </div>
            <div class="header-top" style="background-color:#f6f8fa; border-top:0px;">
            </div>
            <!--.header-bottom-->
            <?php include_once 'header-searchform.php' ?>
        </header>
        <!-- #masthead -->

        <?php include_once 'header-banner.php' ?>

        <div class="main-content">
            <div class="container">
                <div id="content" class="main-content-inner">