<?php
/**
 * Common Header Banner.
 */
 $home_slider_video_height = '';
 $hm_v_height = of_get_option('home_slider_video_height');
 if(!empty($hm_v_height)) {
     $home_slider_video_height = 'height: '.$hm_v_height.'px' ;
 }

?>
<section style="background-color:#0a71b1">
<div class="container" style="color:#fff; padding-bottom:25px;">
<h1>Welcome to TopRankers Blog</h1>
<p style="font-size: 15px">Get latest updates on all upcoming competative exams and job alerts along with<br />
study material, tips and tricks to crack all competative exams.</p>
</div>
</section>