<?php /* Template Name: TopRankers */ ?>


<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package ascent
 */

get_header(); ?>

  <div class="row">
    <div class="col-sm-12 col-md-8">
      <div class="row display-flex">
        <div class="col-lg-6">
          <div class="thumbnail" style="background-color:#fff;">
            <img style="border-radius: 5px;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2018/03/05173046/SSC-CHSL-Exam-Analysis-400x200.jpg"
              alt="null">
            <div class="caption">
              <a href="#" style="text-decoration: none; color:#337ab7;">
                <strong>Exam Analysis, SSC Resource </strong>
              </a>
              <h3>SSC CHSL Exam Analysis 5th March 2018 Slot 1</h3>
              <p>by
                <a href="#" style="text-decoration: none; color:#337ab7;">Sikha</a> on March 5,2018</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="thumbnail" style="background-color:#fff;">
            <img style="border-radius: 5px;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2018/03/05174624/SSC-CPO-Selection-Process-2018-400x200.jpg"
              alt="null">
            <div class="caption">
              <a href="#" style="text-decoration: none; color:#337ab7;">
                <strong>Selection Process, SSC Resources</strong>
              </a>
              <h3>SSC CPO Selection Process 2018 – All That You Need To Know</h3>
              <p>by
                <a href="#" style="text-decoration: none; color:#337ab7;">shashi prakash</a> on March 5,2018</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row display-flex">
        <div class="col-lg-6">
          <div class="thumbnail" style="background-color:#fff;">
            <img style="border-radius: 5px;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2018/03/05165953/Indian-Coast-Guard-Admit-Card-400x200.jpg"
              alt="null">
            <div class="caption">
              <a href="#" style="text-decoration: none; color:#337ab7;">
                <strong>Admit Card</strong>
              </a>
              <h3>Indian Coast Guard Admit Card for Navik DB 2018 – Direct Link to Download Call Letter</h3>
              <p>by
                <a href="#" style="text-decoration: none; color:#337ab7;">TopRankers</a> on March 5,2018</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="thumbnail" style="background-color:#fff;">
            <img style="border-radius: 5px;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2018/03/05173046/SSC-CHSL-Exam-Analysis-400x200.jpg"
              alt="null">
            <div class="caption">
              <a href="#" style="text-decoration: none; color:#337ab7;">
                <strong>Exam Analysis, SSC Resources</strong>
              </a>
              <h3>SSC CHSL Exam Feedback – How was your exam?</h3>
              <p>by
                <a href="#" style="text-decoration: none; color:#337ab7;">Rohit</a> on March 5,2018</p>

            </div>
          </div>
        </div>
      </div>
      <div class="row display-flex">
        <div class="col-lg-6">
          <div class="thumbnail" style="background-color:#fff;">
            <img style="border-radius: 5px;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2018/03/05002209/Canara-Bank-PO-Answer-Key-400x200.jpg"
              alt="null">
            <div class="caption">
              <a href="#" style="text-decoration: none; color:#337ab7;">
                <strong>Answer key, Bank Exams, Canara Bank</strong>
              </a>
              <h3>Canara Bank PO Answer Key
                <br>2018 – Download Paper Solutions in PDF</h3>
              <p>by
                <a href="#" style="text-decoration: none; color:#337ab7;">Vineesh</a> on March 4,2018</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="thumbnail" style="background-color:#fff;">
            <img style="border-radius: 5px;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2018/03/05001415/31-400x200.jpg"
              alt="null">
            <div class="caption">
              <a href="#" style="text-decoration: none; color:#337ab7;">
                <strong>Exam Analysis, SSC Resources</strong>
              </a>
              <h3>SSC CHSL Tier-I Exam
                <br> Analysis 4th March 2018 Slot 2</h3>
              <p>by
                <a href="#" style="text-decoration: none; color:#337ab7;">Sudhir</a> on March 5,2018</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row display-flex">
        <div class="col-lg-6">
          <div class="thumbnail" style="background-color:#fff;">
            <img style="border-radius: 5px;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2018/03/04224536/Canara-Bank-PO-Exam-Analysis-4th-March-2018-Slot-2-400x200.jpg"
              alt="null">
            <div class="caption">
              <a href="#" style="text-decoration: none; color:#337ab7;">
                <strong>Canara Bank, Exam Analysis</strong>
              </a>
              <h3>Canara Bank PO Exam
                <br> Analysis 4th March 2018 Slot 2</h3>
              <p>by
                <a href="#" style="text-decoration: none; color:#337ab7;">Vineesh</a> on March 4,2018</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="thumbnail" style="background-color:#fff;">
            <img style="border-radius: 5px;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2018/03/05173046/SSC-CHSL-Exam-Analysis-400x200.jpg"
              alt="null">
            <div class="caption">
              <a href="#" style="text-decoration: none; color:#337ab7;">
                <strong>Exam Analysis, SSC Resource </strong>
              </a>
              <h3>SSC CHSL Exam Analysis 5th March 2018 Slot 1</h3>
              <p>by
                <a href="#" style="text-decoration: none; color:#337ab7;">Sikha</a> on March 5,2018</p>
            </div>
          </div>
        </div>
      </div>

      <div style="margin: 50px; text-align:center;">
        <button class="btn-lg btn-primary" style="border-radius: 50px; background:#206ab3; padding: 15px 30px 15px 30px;">
          <strong>Load More</strong>
        </button>
      </div>
      <div class="col-sm-7" style="display:inline;">
        <img style="border-radius: 5px;" style="float:left;" src="https://s3-ap-south-1.amazonaws.com/tr-wp-media-uploads/exams/wp-content/uploads/2017/10/13170920/updates_ic.svg"
        />
        <h4>&nbsp;Be the first one to
          <b>get the updates</b> on
          <br> &nbsp;many other intresting reads. </h4>
      </div>
      <div class="col-sm-5" style="display:inline;padding:10px;">
        <input type="email" name="email" id="email" placeholder="Enter your E-mail" />
        <input style="width:100%;background-color:#597bb9" type="submit" value="SUBMIT">
      </div>
    </div>

  <div class="col-sm-12 col-md-4">
    <?php get_sidebar(); ?>
  </div>
  </div>
  <?php get_footer(); ?>